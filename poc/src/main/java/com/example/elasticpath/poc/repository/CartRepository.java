package com.example.elasticpath.poc.repository;

import com.example.elasticpath.poc.entity.EpCartEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository <EpCartEntity, String>  {


}
