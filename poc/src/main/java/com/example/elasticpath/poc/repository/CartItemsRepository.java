package com.example.elasticpath.poc.repository;

import com.example.elasticpath.poc.entity.EpCartItemsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CartItemsRepository extends JpaRepository<EpCartItemsEntity, String> {


    @Transactional
    @Modifying
    @Query ("delete from EpCartItemsEntity where ITEM_ID not in (:itemList) and CART_ID = :cartId")
    public void deleteCartItems (@Param("itemList") List<String> itemList, @Param("cartId") String cartId);


}
