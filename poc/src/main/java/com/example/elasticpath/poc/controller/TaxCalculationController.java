package com.example.elasticpath.poc.controller;

import com.example.elasticpath.poc.exceptions.CredentialsTokenException;
import com.example.elasticpath.poc.utils.AuthenticationUtil;
import com.example.elasticpath.poc.utils.DatabaseUtil;
import com.example.elasticpath.poc.utils.HttpUtil;
import org.apache.http.HttpException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
public class TaxCalculationController {

    @Autowired
    private DatabaseUtil databaseUtil;
    @Autowired
    private AuthenticationUtil authenticationUtil;
    @Autowired
    private HttpUtil httpUtil;

    @Value("${baseUrl}")
    private String baseUrl;

    public final static Logger logger = LoggerFactory.getLogger(TaxCalculationController.class);


    @PostMapping("/cart/calculateTax")
    public ResponseEntity<String> applyTax(@RequestBody String body) {

        try{
            JSONObject payloadJson = new JSONObject(body).getJSONObject("payload");
            JSONObject dataJson = payloadJson.getJSONObject("data");

            //retrieve cart address (Country, State, Zipcode)
            String cartId = dataJson.getString("id").equals("null") ? null : dataJson.getString("id");
            String state = dataJson.getString("state").equals("null") ? null : dataJson.getString("state");
            String country = dataJson.getString("country").equals("null") ? null : dataJson.getString("country");
            String zipcode = dataJson.getString("zipcode").equals("null") ? null : dataJson.getString("zipcode");
            logger.info("cartId:" + cartId + " state: " + state + " country: " + country + " zipcode: " + zipcode);

            if (cartId == null){
                logger.error("cartID is null");
                return new ResponseEntity<>("cartID is null",HttpStatus.BAD_REQUEST);
            }

            if(state == null || country == null || zipcode == null){
                logger.info("address data incomplete: taxes will not be calculated");
                return new ResponseEntity<>("Tax not calculated: address data incomplete",HttpStatus.OK);
            }

            //calculate tax rate (es. 0.125 -> 12.5% tax rate)
            //TODO implement exchange data with a third party tax service
            double taxRate = getTaxRate(state, country, zipcode);
            logger.info("taxRate: " + taxRate);

            boolean isTaxRateZero = taxRate == 0;

            //retrieve EPCC authentication token -> {{authUrl}}/oauth/access_token
            String accessToken = authenticationUtil.getAccessToken();

            //retrieve cart items data
            JSONArray cartItemsData;
            try{
                cartItemsData = payloadJson.getJSONObject("included").getJSONArray("items");
            }
            catch (JSONException e){
                logger.info("No items exist in the cart");
                return new ResponseEntity<>("Tax not calculated: no items exist in the cart",HttpStatus.OK);
            }

            //delete cartItems from DB if relative products have been deleted from cart
            List<String> cartItemsID = getCartItemsIdFromJson(cartItemsData);
            databaseUtil.deleteCartItems(cartItemsID, cartId);

            //apply, update or delete tax for every cart item
            for (int i=0; i<cartItemsData.length(); i++){

                JSONObject cartItem = cartItemsData.getJSONObject(i);
                String itemId = cartItem.getString("id");
                logger.info ("cart item ID: " + itemId);

                if(taxExists(cartItem)){

                    //TAX UPDATE/DELETE URL -> {{baseUrl}}/carts/{{cartID}}/items/{{cartitemID}}/taxes/{{taxitemID}}

                    //make a PUT request if taxRate > 0 to update a tax item, otherwise make a DELETE request to delete a tax item if taxRate = 0
                    JSONArray taxesData = cartItem.getJSONObject("relationships").getJSONObject("taxes").getJSONArray("data");
                    //EPCC allows to apply up to 5 tax items per cart item: FOR cycle is implemented even though for this POC we consider just one tax item per cart item
                    for (int j=0; j<taxesData.length(); j++){
                        String taxID = taxesData.getJSONObject(j).getString("id");
                        String urlUpdate = baseUrl + "/carts/"+cartId+"/items/"+itemId+"/taxes/"+taxID;
                        if (isTaxRateZero){
                            //make a DELETE request to remove tax item
                            httpUtil.sendHttpDelete(urlUpdate, accessToken, itemId);
                        }
                        else{
                            //make a PUT request to update tax item
                            httpUtil.sendHttpPut(urlUpdate, country, taxRate, accessToken, itemId);
                        }
                    }

                    saveCartItem (cartId, itemId, accessToken);
                }
                else{
                    //TAX ADD URL -> {{baseUrl}}/carts/{{cartID}}/items/{{cartitemID}}/taxes
                    String urlPost = baseUrl + "/carts/"+cartId+"/items/"+itemId+"/taxes";

                    if(isTaxRateZero){
                        logger.info("taxRate is zero and item has no tax applied: do nothing");
                        saveCartItem (cartId, itemId, accessToken);
                        continue;
                    }

                    //make a POST request when taxRate is > 0 to create a tax item
                    httpUtil.sendHttpPost(urlPost, country, taxRate, accessToken);
                    saveCartItem (cartId, itemId, accessToken);
                }
            }

            databaseUtil.saveCart(cartId, country, state, zipcode, taxRate);

            logger.info("Tax applied successfully");
            return new ResponseEntity<>("Tax applied successfully",HttpStatus.OK);
        }
        catch (JSONException jsonException){
            logger.error("Encountered an error while parsing JSON");
            jsonException.printStackTrace();
            return new ResponseEntity<>("Encountered an error while parsing JSON",HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (UnsupportedEncodingException e1) {
            logger.error("Encountered an error of type UnsupportedEncodingException");
            e1.printStackTrace();
            return new ResponseEntity<>("Encountered an error of type UnsupportedEncodingException",HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IOException e2) {
            logger.error("Encountered an error of type IOException");
            e2.printStackTrace();
            return new ResponseEntity<>("Encountered an error of type IOException",HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (CredentialsTokenException cte){
            return new ResponseEntity<>(cte.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch(HttpException he){
            return new ResponseEntity<>(he.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e){
            logger.error("Encountered a generic error");
            e.printStackTrace();
            return new ResponseEntity<>("Generic error",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    //simulate tax rate calculation: get a number between 0.0 and 0.999
    public double getTaxRate (String state, String country, String zipcode){

        double value = new Random().nextInt(1000);
        return value/1000;
    }

    public boolean taxExists (JSONObject cartItem) {

        try{
            if(cartItem.has("relationships")){
                if(cartItem.getJSONObject("relationships").has("taxes")){
                    logger.info("Item " + cartItem.getString("id") + " has already a tax applied");
                    return true;
                }
            }
            logger.info("Item " + cartItem.getString("id") + " has no tax applied yet");
            return false;
        }
        catch (Exception e){
            return false;
        }
    }

    public List<String> getCartItemsIdFromJson(JSONArray cartItemsData) throws JSONException {

        List<String> cartItemsIDList = new ArrayList<>();

        for (int i=0; i<cartItemsData.length(); i++) {

            JSONObject cartItem = cartItemsData.getJSONObject(i);
            String itemId = cartItem.getString("id");
            cartItemsIDList.add(itemId);
        }

        return cartItemsIDList;
    }

    //save cart item data to DB
    public void saveCartItem (String cartId, String itemId, String accessToken) throws IOException, HttpException, JSONException {

        String urlGetCartItems = baseUrl + "/carts/" + cartId + "/items/";
        JSONObject cartItemJsonObject = httpUtil.getUpdatedCartItem(urlGetCartItems, itemId, accessToken);
        //custom items are not linked to a product
        String productId = cartItemJsonObject.has("product_id") ? cartItemJsonObject.getString("product_id") : null;
        String sku = cartItemJsonObject.has("sku") ? cartItemJsonObject.getString("sku") : null;
        int quantity = cartItemJsonObject.getInt("quantity");
        String currency = cartItemJsonObject.getJSONObject("unit_price").getString("currency");
        //if a product has the property "includes_tax" set to TRUE, EPCC performs a backward calculation of taxes meaning the original product price is not affected during checkout
        String priceIncludesTax = cartItemJsonObject.getJSONObject("unit_price").getBoolean("includes_tax") == true ? "Y" : "N";
        double unitPrice = cartItemJsonObject.getJSONObject("meta").getJSONObject("display_price").getJSONObject("without_tax").getJSONObject("unit").getDouble("amount") / 100;
        double unitTax = cartItemJsonObject.getJSONObject("meta").getJSONObject("display_price").getJSONObject("tax").getJSONObject("unit").getDouble("amount") / 100;
        double subtotalPrice = cartItemJsonObject.getJSONObject("meta").getJSONObject("display_price").getJSONObject("without_tax").getJSONObject("value").getDouble("amount") / 100;
        double totalTax = cartItemJsonObject.getJSONObject("meta").getJSONObject("display_price").getJSONObject("tax").getJSONObject("value").getDouble("amount") / 100;
        double totalPrice = cartItemJsonObject.getJSONObject("meta").getJSONObject("display_price").getJSONObject("with_tax").getJSONObject("value").getDouble("amount") / 100;

        databaseUtil.saveCartItem(itemId, productId, cartId, sku, quantity, currency, priceIncludesTax, unitPrice, unitTax, subtotalPrice, totalTax, totalPrice);

    }
}
