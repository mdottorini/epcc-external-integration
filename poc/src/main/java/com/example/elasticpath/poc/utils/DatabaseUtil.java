package com.example.elasticpath.poc.utils;

import com.example.elasticpath.poc.entity.EpCartEntity;
import com.example.elasticpath.poc.entity.EpCartItemsEntity;
import com.example.elasticpath.poc.repository.CartRepository;
import com.example.elasticpath.poc.repository.CartItemsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DatabaseUtil {

    public final static Logger logger = LoggerFactory.getLogger(DatabaseUtil.class);

    @Autowired
    private CartRepository cartDao;
    @Autowired
    private CartItemsRepository cartItemsDao;


    public void saveCart(String cartId, String country, String state, String zipcode, double taxRate) {

        logger.info("Save cart data on DB");
        EpCartEntity epCartEntity = new EpCartEntity();
        epCartEntity.setCartId(cartId);
        epCartEntity.setCountry(country);
        epCartEntity.setState(state);
        epCartEntity.setZipcode(zipcode);
        epCartEntity.setTaxrate(taxRate);
        cartDao.save(epCartEntity);
        logger.info("Cart data saved on DB");
    }


    public void deleteCartItems (List<String> cartItemsID, String cartId){

        logger.info("delete cartItems from DB");
        logger.info("cartItemsID actually on cart: " + cartItemsID);
        logger.info("cartId: " + cartId);
        cartItemsDao.deleteCartItems(cartItemsID, cartId);
        logger.info("cartItems from DB successfully deleted");
    }


    public void saveCartItem(String itemId, String productId, String cartId, String sku, int quantity, String currency,
                             String priceIncludesTax, double unitPrice, double unitTax, double subtotalPrice, double totalTax, double totalPrice){

        logger.info("save cart item data on DB");
        EpCartItemsEntity epCartItemsEntity = new EpCartItemsEntity();
        epCartItemsEntity.setItemId(itemId);
        epCartItemsEntity.setProductId(productId);
        epCartItemsEntity.setCartId(cartId);
        epCartItemsEntity.setSku(sku);
        epCartItemsEntity.setQuantity(quantity);
        epCartItemsEntity.setCurrency(currency);
        epCartItemsEntity.setPriceIncludesTax(priceIncludesTax);
        epCartItemsEntity.setUnitPrice(unitPrice);
        epCartItemsEntity.setUnitTax(unitTax);
        epCartItemsEntity.setSubtotalPrice(subtotalPrice);
        epCartItemsEntity.setTotalTax(totalTax);
        epCartItemsEntity.setTotalPrice(totalPrice);
        cartItemsDao.save(epCartItemsEntity);
        logger.info("cart item on DB saved!");

    }




}
