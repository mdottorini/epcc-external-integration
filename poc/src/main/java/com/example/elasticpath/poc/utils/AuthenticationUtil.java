package com.example.elasticpath.poc.utils;

import com.example.elasticpath.poc.exceptions.CredentialsTokenException;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AuthenticationUtil {

    @Value("${authUrl}")
    private String authUrl;
    @Value("${clientID}")
    private String clientID;
    @Value("${clientSecret}")
    private String clientSecret;
    @Value("${clientGrantType}")
    private String clientGrantType;

    public final static Logger logger = LoggerFactory.getLogger(AuthenticationUtil.class);



    public String getAccessToken() throws IOException, JSONException, CredentialsTokenException {

        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(authUrl + "/oauth/access_token");
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("client_id", clientID));
        params.add(new BasicNameValuePair("client_secret", clientSecret));
        params.add(new BasicNameValuePair("grant_type", clientGrantType));
        httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        CloseableHttpResponse response = client.execute(httpPost);
        if(response.getStatusLine().getStatusCode() != 200){
            client.close();
            logger.error("Error on retrieving token authentication");
            throw new CredentialsTokenException("Error on retrieving token authentication");
        }

        String responseAccessToken = EntityUtils.toString(response.getEntity());
        client.close();

        logger.info("responseAccessToken: " + responseAccessToken);
        String accessToken = new JSONObject(responseAccessToken).getString("access_token");
        return accessToken;

    }
}
