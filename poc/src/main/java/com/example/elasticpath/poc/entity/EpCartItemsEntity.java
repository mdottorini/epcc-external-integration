package com.example.elasticpath.poc.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EP_CARTITEMS")
public class EpCartItemsEntity {


    @Id
    @Column(name = "ITEM_ID")
    private String itemId;

    @Column(name = "PRODUCT_ID")
    private String productId;

    @Column(name = "CART_ID")
    private String cartId;

    @Column(name = "SKU")
    private String sku;

    @Column(name = "QUANTITY")
    private int quantity;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "PRICE_INCLUDES_TAX")
    private String priceIncludesTax;

    @Column(name = "UNIT_PRICE")
    private double unitPrice;

    @Column(name = "UNIT_TAX")
    private double unitTax;

    @Column(name = "SUBTOTAL_PRICE")
    private double subtotalPrice;

    @Column(name = "TOTAL_TAX")
    private double totalTax;

    @Column(name = "TOTAL_PRICE")
    private double totalPrice;




    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPriceIncludesTax() {
        return priceIncludesTax;
    }

    public void setPriceIncludesTax(String priceIncludesTax) {
        this.priceIncludesTax = priceIncludesTax;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getUnitTax() {
        return unitTax;
    }

    public void setUnitTax(double unitTax) {
        this.unitTax = unitTax;
    }

    public double getSubtotalPrice() {
        return subtotalPrice;
    }

    public void setSubtotalPrice(double subtotalPrice) {
        this.subtotalPrice = subtotalPrice;
    }

    public double getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(double totalTax) {
        this.totalTax = totalTax;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

}
