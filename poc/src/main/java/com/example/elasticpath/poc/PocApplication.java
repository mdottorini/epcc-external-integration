package com.example.elasticpath.poc;

import com.example.elasticpath.poc.utils.AuthenticationUtil;
import com.example.elasticpath.poc.utils.DatabaseUtil;
import com.example.elasticpath.poc.utils.HttpUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class PocApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocApplication.class, args);
	}


	@Bean
	public DatabaseUtil databaseUtil (){
		return new DatabaseUtil();
	}

	@Bean
	AuthenticationUtil authenticationUtil (){
		return new AuthenticationUtil();
	}

	@Bean
	HttpUtil httpUtil (){
		return new HttpUtil();
	}

}
