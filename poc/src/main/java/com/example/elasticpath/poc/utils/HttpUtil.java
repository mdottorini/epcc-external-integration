package com.example.elasticpath.poc.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class HttpUtil {

    public final static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    public void sendHttpPost(String urlPost, String country, double taxRate, String accessToken) throws JSONException, IOException, HttpException {

        logger.info ("add tax item url: " + urlPost);

        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(urlPost);
        httpPost.addHeader("Authorization","Bearer " + accessToken);
        httpPost.addHeader("content-type","application/json");
        httpPost.setEntity(createBody(country, taxRate));
        CloseableHttpResponse response = client.execute(httpPost);
        if(response.getStatusLine().getStatusCode() != 201){
            logger.error("Could not create tax item");
            client.close();
            response.close();
            throw new HttpException("Could not create tax item");
        }
        String responseString = EntityUtils.toString(response.getEntity());
        client.close();
        response.close();
        logger.info("responseString: " + responseString);
    }


    public void sendHttpPut(String urlUpdate, String country, double taxRate, String accessToken, String itemId) throws JSONException, IOException, HttpException {

        logger.info ("update tax item url: " + urlUpdate);

        CloseableHttpClient client = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(urlUpdate);
        httpPut.addHeader("Authorization","Bearer " + accessToken);
        httpPut.addHeader("content-type","application/json");
        httpPut.setEntity(createBody(country, taxRate));
        CloseableHttpResponse response = client.execute(httpPut);
        if(response.getStatusLine().getStatusCode() != 200){
            logger.error("Could not update tax item");
            client.close();
            response.close();
            throw new HttpException("Could not update tax item");
        }
        String responseString = EntityUtils.toString(response.getEntity());
        client.close();
        response.close();
        logger.info("tax for "+ itemId +" updated!");
        logger.info("responseString: " + responseString);
    }


    public void sendHttpDelete(String urlUpdate, String accessToken, String itemId) throws IOException, HttpException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpDelete httpDelete = new HttpDelete(urlUpdate);
        logger.info ("delete tax item url: " + urlUpdate);
        httpDelete.addHeader("Authorization","Bearer " + accessToken);
        httpDelete.addHeader("content-type","application/json");
        CloseableHttpResponse response = client.execute(httpDelete);
        if(response.getStatusLine().getStatusCode() != 204){
            logger.error("Could not remove tax item");
            client.close();
            response.close();
            throw new HttpException("Could not remove tax item");
        }
        client.close();
        response.close();
        logger.info("tax for "+ itemId +" deleted!");
    }



    public HttpEntity createBody(String country, double taxRate) throws JSONException, UnsupportedEncodingException {
        JSONObject taxDataJson = new JSONObject();
        taxDataJson.put("type","tax_item");
        taxDataJson.put("name","tax_item");
        taxDataJson.put("jurisdiction",country);
        taxDataJson.put("code","TAX");
        taxDataJson.put("rate",taxRate);
        JSONObject taxJson = new JSONObject().put("data", taxDataJson);
        logger.info("taxJson: " + taxJson);
        return new StringEntity(taxJson.toString());
    }


    public JSONObject getUpdatedCartItem (String url, String itemId, String accessToken) throws IOException, HttpException, JSONException {

        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        httpGet.addHeader("Authorization","Bearer " + accessToken);
        httpGet.addHeader("content-type","application/json");
        CloseableHttpResponse response = client.execute(httpGet);
        if(response.getStatusLine().getStatusCode() != 200){
            logger.error("Could not get cart items");
            client.close();
            response.close();
            throw new HttpException("Could not get cart items");
        }
        String responseString = EntityUtils.toString(response.getEntity());
        client.close();
        response.close();

        JSONArray cartItemsData = new JSONObject(responseString).getJSONArray("data");
        JSONObject cartItem = null;

        for (int i=0; i<cartItemsData.length(); i++){

            if(cartItemsData.getJSONObject(i).getString("id").equals(itemId)){
                cartItem = cartItemsData.getJSONObject(i);
                break;
            }
        }

        return cartItem;
    }


}
