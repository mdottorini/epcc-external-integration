package com.example.elasticpath.poc.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EP_CART")
public class EpCartEntity {

    @Id
    @Column (name = "CART_ID")
    private String cartId;

    @Column (name = "COUNTRY")
    private String country;

    @Column (name = "STATE")
    private String state;

    @Column (name = "ZIPCODE")
    private String zipcode;

    @Column (name = "TAXRATE")
    private double taxrate;


    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }
}
