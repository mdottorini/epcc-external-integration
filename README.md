# EPCC TAX INTEGRATION #

The aim of this POC is to integrate a third-party service on EPCC for calculating taxes. 



## Introduction

The POC consists mainly on a REST controller that exposes 
the resource "cart/calculateTax", used by EPCC through the use of a Webhook. 

By means of webhooks, EPCC can listen on specific events 
and send notification data to a specified URL, so that custom logic/functions can be triggered outside of Commerce Cloud: 
in this case, a webhook has to be created on EPCC that listens on the **cart.updated** event
and the registered URL must point to the resource handled by the REST controller defined on this POC.

The REST controller retrieves address data from the payload sent by Webhook and exchange it with a tax external service in order to get
a tax rate to be applied to the cart items.

The address data at the cart level do not exist by default in EPCC, so the Cart Core Flow has been extended in order to add to the cart 
the information of the address: 3 fields have been added to the cart core flow (State, Country, Zipcode).

The communication with a tax external service has been simulated in this POC: this means that the tax rate is randomly calculated.



## Tasks performed by the POC

### 1. Read data from webhook payload

Once the cart is updated on EPCC (add/remove products, change cart data), the webhook integration automatically sends 
the updated cart data to the registered URL, through a POST request. The REST controller parses the JSON body
and saves the data about address (country, state, zipcode) and cart items (products added to the cart) that are actually present on the cart.

### 2. Retrieve tax rate

This part, as previously mentioned, is simulated so there is not a real exchange data with an external service but 
a local calculation of the tax rate: that is, a generation of a double number between 0.0 and 0.999, so that, for instance,
the number 0.125 means a tax rate of 12.5% that has to be applied to the cart items.

### 3. Apply tax rate to cart items

First of all, an access token must be get in order to make the following requests to EPCC by means of its API.
    
Tax rate is applied to every cart item, after having analyzed if it already has a tax applied or not, this means sending to EPCC:
     
a. **POST request**: if a tax item has to be created and linked to the correspondig cart item. This allows to apply tax to a cart item that has no tax yet.
        
b. **PUT request**: if a tax item, previously created for a cart item, has to be updated (with the new tax rate). This updates the existing tax to a cart item.  
    
c. **DELETE request**: if the tax rate is zero, this request deletes the tax item so that no tax is calculated for the cart item.


### 4. Save data on a Database

Information about cart (cartId, address, tax rate) and cart items are persisted on a Database: for cart items belonging to a cart, that means add, update
or remove records on their corresponding table.

In this POC, it is supposed to save data to an Oracle Database. Tables definition are the following:
***
```bash
create table EP_CART (
    
CART_ID  VARCHAR2 (50) PRIMARY KEY,
COUNTRY  VARCHAR2(30),
STATE    VARCHAR2(2),
ZIPCODE  VARCHAR2(5),
TAXRATE  NUMBER(4,3)
    
);
    
create table EP_CARTITEMS (
    
ITEM_ID                 VARCHAR2 (50) PRIMARY KEY,
PRODUCT_ID              VARCHAR2 (50),
CART_ID                 VARCHAR2 (50) REFERENCES EP_CART (CART_ID),
SKU                     VARCHAR2 (50),
QUANTITY                NUMBER,
CURRENCY                VARCHAR2 (5),
PRICE_INCLUDES_TAX      CHAR(1),
UNIT_PRICE              NUMBER (20,2),
UNIT_TAX                NUMBER (20,2),
SUBTOTAL_PRICE          NUMBER (20,2),
TOTAL_TAX               NUMBER (20,2),
TOTAL_PRICE             NUMBER (20,2)
    
); 

```
***